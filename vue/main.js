import Vue from "vue";

import Router from "./router";
import Store from "./store";

import App from "./App";

import "@popperjs/core";
import "bootstrap-icons/font/bootstrap-icons.css";

import axios from "axios";

const _ = require("lodash");

window.bootstrap = require("bootstrap/dist/js/bootstrap.bundle.min");

import "./assets/scss/common.scss";

axios.defaults.baseURL = "http://localhost/api";
axios.defaults.headers["auth"] = Store.getters.getAuthKey;


axios.interceptors.response.use(undefined, (error) => {

    if (error.response.status === 401) {
        Store.dispatch("auth_logout").then(() => {
            Router.push("/login").then();
        });
    }

    return Promise.reject(error);
});

new Vue({
    render: h => h(App),
    router: Router,
    store: Store
}).$mount("#app");

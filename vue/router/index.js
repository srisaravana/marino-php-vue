import Vue from "vue";
import VueRouter from "vue-router";

import PagesRoutesGroup from "./groups/pages";
import UsersRoutesGroup from "./groups/users";
import Login from "../views/Login";

import Store from "../store/index";

Vue.use(VueRouter);

const routes = [

    {
        path: "/login",
        name: "Login",
        component: Login,
    },

    ...PagesRoutesGroup,
    ...UsersRoutesGroup,
];

const Router = new VueRouter({
    routes: routes,
});


Router.beforeEach((to, from, next) => {

    const userType = Store.getters.getUserType;
    const isLoggedIn = Store.getters.getLoginStatus;

    if (to.matched.some(record => record.meta.requiresAuth)) {

        if (to.meta.adminOnly) {
            if (userType !== "ADMIN") {
                next("/login");
            }
        }

        if (isLoggedIn) {
            next();
        } else {
            next("/login");
        }

    } else {
        next();
    }

});

export default Router;

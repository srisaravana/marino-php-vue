import ManageUsers from "../../views/users/ManageUsers";
import CreateUser from "../../views/users/CreateUser";
import EditUser from "../../views/users/EditUser";

const UsersRoutesGroup = [
    {
        path: "/users",
        name: "ManageUsers",
        component: ManageUsers,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
    {
        path: "/users/create",
        name: "CreateUser",
        component: CreateUser,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
    {
        path: "/users/edit/:id",
        name: "EditUser",
        component: EditUser,
        meta: {
            requiresAuth: true,
            adminOnly: true,
        }
    },
];

export default UsersRoutesGroup;

import Home from "../../views/pages/Home";
import About from "../../views/pages/About";

const PagesRoutesGroup = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },

    {
        path: "/about",
        name: "About",
        component: About,
        meta: {
            requiresAuth: true,
        }
    },
];

export default PagesRoutesGroup;

import axios from "axios";


let Users = {

    state: {

        user: {},
        users: [],
        userRoles: {
            "USER": "User",
            "MANAGER": "Manager",
            "ADMIN": "Administrator",
        }

    },

    getters: {

        getUser(state) { return state.user; },
        getUsers(state) { return state.users; },

        getUserRoles(state) { return state.userRoles; },
    },

    mutations: {

        setUser(state, data) { state.user = data; },
        setUsers(state, data) { state.users = data; },

    },

    actions: {

        async users_fetchAll(context) {

            try {

                const response = await axios.get("users/all.php");
                context.commit("setUsers", response.data.payload.users);

            } catch (e) {
                throw e;
            }

        }, /* fetch all */

        async users_fetch(context, id) {

            try {

                const response = await axios.get(`users/get.php?id=${id}`);
                context.commit("setUser", response.data.payload.user);

            } catch (e) {
                throw e;
            }

        }, /* fetch */

        async users_create(context, params) {

            try {

                await axios.post("users/create.php", params);

            } catch (e) {
                throw e;
            }

        }, /* create */

        async users_update(context, params) {
            try {
                await axios.post("users/update.php", params);
            } catch (e) {
                throw e;
            }
        }, /* update */


        async users_updatePassword(context, params) {

            try {

                await axios.post("users/update-password.php", params);

            } catch (e) {
                throw e;
            }

        }

    },

};

export default Users;

import axios from "axios";


const LOGIN_STATUS = "login-status";
const AUTH_KEY = "auth-key";
const USER = "user-details";

let Auth = {

    state: {

        _storeLoginStatus: localStorage.getItem(LOGIN_STATUS) || "false",
        _storeAuthKey: localStorage.getItem(AUTH_KEY) || "",
        _storeUser: localStorage.getItem(USER) || "{}",

    },
    getters: {

        getLoginStatus(state) {
            return state._storeLoginStatus === "true"
        },

        getLoggedInUser(state) {
            return JSON.parse(state._storeUser);
        },

        getAuthKey(state) {
            return state._storeAuthKey;
        },

        getUserType(state) {
            return JSON.parse(state._storeUser)["role"] || "NONE";
        }

    },
    mutations: {

        loginSuccess(state) {
            state._storeLoginStatus = localStorage.getItem(LOGIN_STATUS) || "false";
            state._storeAuthKey = localStorage.getItem(AUTH_KEY) || "";
            state._storeUser = localStorage.getItem(USER) || "{}";

        },

        logoutSuccess(state) {
            state._storeLoginStatus = "false";
            state._storeAuthKey = "";
            state._storeUser = "{}";
        }

    },
    actions: {


        async auth_login(context, loginData) {

            try {

                const response = await axios.post("auth/login.php", loginData);

                const key = response.data.payload.auth_key;
                const user = response.data.payload.user;

                localStorage.setItem(AUTH_KEY, key);
                localStorage.setItem(USER, JSON.stringify(user));
                localStorage.setItem(LOGIN_STATUS, "true");

                axios.defaults.headers["auth"] = key;

                context.commit("loginSuccess");


            } catch (e) {
                throw e;
            }

        },

        async auth_logout(context) {
            localStorage.setItem(AUTH_KEY, "");
            localStorage.setItem(USER, "{}");
            localStorage.setItem(LOGIN_STATUS, "false");

            context.commit("logoutSuccess");

        }

    },

};

export default Auth;

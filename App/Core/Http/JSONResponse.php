<?php


namespace App\Core\Http;


use Exception;

class JSONResponse
{

    public array $payload;
    public int $statusCode;

    public function __construct($payload, $statusCode)
    {
        $this->payload = $payload;
        $this->statusCode = $statusCode;
    }

    public function response()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Origin, Methods");
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Max-Age: 3600");
        header("Content-Type: application/json; charset=UTF-8");

        http_response_code($this->statusCode);
        echo json_encode(["payload" => $this->payload, "status" => $this->statusCode]);

    }

    public static function validResponse($payload)
    {

        if (is_array($payload)) {
            $response = new JSONResponse($payload, 200);
        } else {
            $response = new JSONResponse(["data" => $payload], 200);
        }

        $response->response();
    }

    public static function invalidResponse($payload, $statusCode = 400)
    {
        if (is_array($payload)) {
            $response = new JSONResponse($payload, $statusCode);
        } else {
            $response = new JSONResponse(["data" => $payload], $statusCode);
        }

        $response->response();
    }

    public static function exceptionResponse(Exception $exception)
    {
        self::invalidResponse(["error" => $exception->getMessage()]);
    }

}

<?php

declare(strict_types=1);

namespace App\Core\Http;


use App\Models\AuthKey;

class Auth
{

    public static function authenticate(): bool
    {

        /* check if it can be authenticated, otherwise throw an exception */

        $authKey = Request::getAuthKey();

        if (!empty($authKey)) {
            if (AuthKey::validateAuthKey($authKey)) return true;
        }

        JSONResponse::invalidResponse("Authentication failed", 401);
        die();
    }

}

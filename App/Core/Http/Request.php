<?php

declare(strict_types=1);

namespace App\Core\Http;


use Exception;

class Request
{

    public const METHOD_GET = "GET";
    public const METHOD_POST = "POST";

    public static function method(): string
    {
        return $_SERVER["REQUEST_METHOD"];
    }

    public static function scheme(): string
    {
        return $_SERVER["REQUEST_SCHEME"];
    }

    public static function host(): string
    {
        return $_SERVER["HTTP_HOST"];
    }


    /**
     * @throws Exception
     */
    public static function validateRequestMethod(string $acceptedMethod): bool
    {
        $method = Request::method();
        if ($method == $acceptedMethod) return true;

        throw new Exception("$method not accepted");
    }


    /**
     * @throws Exception
     */
    private static function getParam(string $key, bool $required = false)
    {

        $axios = self::getAxiosData();

        $_REQUEST = array_merge($axios, $_REQUEST);

        /* try to get the data from request */
        if (isset($_REQUEST[$key])) return $_REQUEST[$key];

        /* if data is not found, and is required then throw error */
        if ($required) throw new Exception("Field ($key) is required");

        /* finally, if no data is found and it is not required, just return null */
        return null;
    }


    /**
     * @throws Exception
     */
    public static function getAsInteger(string $key, bool $required = false): ?int
    {
        $data = self::getParam($key, $required);

        if ($data == "0") return 0;

        if (!empty($data)) {

            if (filter_var($data, FILTER_VALIDATE_INT)) {
                return (int)$data;
            }
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public static function getAsFloat(string $key, bool $required = false): ?float
    {
        $data = self::getParam($key, $required);
        if ($data == "0") return 0.0;

        if (!empty($data)) {
            if (filter_var($data, FILTER_VALIDATE_FLOAT)) {
                return (float)$data;
            }
        }

        return null;
    }


    /**
     * @throws Exception
     */
    public static function getAsString(string $key, bool $required = false): ?string
    {
        $data = self::getParam($key, $required);

        if (!empty($data)) {
            return filter_var($data, FILTER_SANITIZE_STRING);
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public static function getAsRawString(string $key, bool $required = false): ?string
    {
        return self::getParam($key, $required);
    }

    /**
     * @throws Exception
     */
    public static function getAsBoolean(string $key, bool $required = false): ?bool
    {
        $data = self::getParam($key, $required);

        if ($data == "0") return false;

        if (!empty($data)) {
            return filter_var($data, FILTER_VALIDATE_BOOLEAN);
        }

        return null;
    }


    private static function getAxiosData(): array
    {

        $data = json_decode(file_get_contents("php://input"), true);
        if (!is_null($data)) return $data;
        return [];

    }

    public static function getAuthKey(string $key = "HTTP_AUTH"): string
    {
        if (isset($_SERVER[$key])) return $_SERVER[$key];
        return "";
    }

    /**
     * Enable CORS support. This method can be hooked into bootstrap call
     */
    public static function cors()
    {

        // Allow from any origin
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            // Decide if the origin in $_SERVER["HTTP_ORIGIN"] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$_SERVER["HTTP_ORIGIN"]}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {

            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");

            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]}");

            exit(0);
        }

    }


}

<?php


namespace App\Models;


use App\Core\Database\Database;
use Carbon\Carbon;
use Exception;

class AuthKey
{

    private const TABLE = "users_auth_keys";

    public ?int $user_id;
    public ?string $auth_key, $valid_till;

    public ?User $user;

    /**
     * @throws Exception
     */
    public static function generateAuthKey(User $user): string
    {

        if (is_null(User::find($user->id))) throw new Exception("User does not exist");


        /*
         * check if there is an auth key in the database, if there is one,
         * check if it is still valid
         * */
        $authKey = AuthKey::getAuthKey($user);
        if (!is_null($authKey)) {

            $now = Carbon::now();
            $keyValidTill = new Carbon($authKey->valid_till);

            if ($now->greaterThan($keyValidTill)) {
                /* key is expired */
                self::deleteAuthKey($user);
            } else {
                return $authKey->auth_key;
            }

        }


        /*
         * generate new auth key and store it in the database
         * */

        $key = md5(microtime() . rand());
        $validTill = Carbon::now()->add(30, "day")->toDateTimeString();

        $db = Database::instance();
        $statement = $db->prepare("insert into users_auth_keys(user_id, auth_key, valid_till) values(?,?,?)");

        $result = $statement->execute([$user->id, $key, $validTill]);

        if ($result) return $key;

        throw new Exception("Failed to generate auth key");

    }

    public static function getAuthKey(User $user): ?AuthKey
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users_auth_keys where user_id=?");
        $statement->execute([$user->id]);

        /** @var AuthKey $result */
        $result = $statement->fetchObject(self::class);

        if (!empty($result)) return $result;
        return null;

    }

    public static function deleteAuthKey(User $user): bool
    {
        return Database::delete(self::TABLE, "user_id", $user->id);
    }

    public static function validateAuthKey(string $authKey): bool
    {

        $db = Database::instance();
        $statement = $db->prepare("select * from users_auth_keys where auth_key=?");
        $statement->execute([$authKey]);

        /** @var AuthKey $result */
        $result = $statement->fetchObject(self::class);

        if (!empty($result)) {

            $now = Carbon::now();
            $validTill = new Carbon($result->valid_till);

            if ($now->greaterThan($validTill)) return false;

            return true;
        }
        return false;

    }

}

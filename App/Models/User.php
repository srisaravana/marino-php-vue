<?php

namespace App\Models;

use App\Core\Database\Database;
use Exception;

class User implements IModel
{

    private const TABLE = "users";

    public ?int $id;
    public ?string $username, $full_name, $password_hash, $password, $email, $role;

    public static function build($array): User
    {
        $object = new self();
        foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find(int $id): ?User
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return User[]|null
     */
    public static function findAll($limit = 1000, $offset = 0): ?array
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class, "full_name");
    }

    /**
     * @return bool|int
     * @throws Exception
     */
    public function insert()
    {

        if (self::usernameAlreadyExist($this->username)) throw new Exception("Username already exist");
        if (self::emailAlreadyExist($this->email)) throw new Exception("Email already exist");

        $hash = password_hash($this->password, PASSWORD_DEFAULT);

        $data = [
            "username" => $this->username,
            "full_name" => $this->full_name,
            "email" => $this->email,
            "role" => $this->role,
            "password_hash" => $hash,
        ];

        return Database::insert(self::TABLE, $data);

    }

    /**
     * @throws Exception
     */
    public function update(): bool
    {

        if ($this->sameUsernameExist()) throw new Exception("Username already exist");
        if ($this->sameEmailExist()) throw new Exception("Email already exist");

        $data = [
            "username" => $this->username,
            "full_name" => $this->full_name,
            "email" => $this->email,
            "role" => $this->role,
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);

    }

    /**
     * @throws Exception
     */
    public function delete()
    {
        throw new Exception("Not implemented");
    }

    public static function userExist($username, $password): ?User
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where username=?");
        $statement->execute([$username]);

        /** @var User $result */
        $result = $statement->fetchObject(self::class);

        if (!empty($result)) {
            if (password_verify($password, $result->password_hash)) {
                return $result;
            }
        }

        return null;

    }

    public static function usernameAlreadyExist(string $username): bool
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where username=?");
        $statement->execute([$username]);

        $result = $statement->fetchObject(self::class);

        if (!empty($result)) return true;
        return false;

    }

    public static function emailAlreadyExist(string $email): bool
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where email=?");
        $statement->execute([$email]);

        $result = $statement->fetchObject(self::class);

        if (!empty($result)) return true;
        return false;
    }

    public function sameUsernameExist(): bool
    {

        $db = Database::instance();
        $statement = $db->prepare("select * from users where username=? and id !=?");
        $statement->execute([$this->username, $this->id]);

        $result = $statement->fetchObject(self::class);

        if (!empty($result)) return true;
        return false;

    }

    public function sameEmailExist(): bool
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where email=? and id !=?");
        $statement->execute([$this->email, $this->id]);

        $result = $statement->fetchObject(self::class);

        if (!empty($result)) return true;
        return false;
    }


    public function updatePassword($newPassword): bool
    {

        $this->password_hash = password_hash($newPassword, PASSWORD_DEFAULT);

        $data = [
            "password_hash" => $this->password_hash
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);

    }

}

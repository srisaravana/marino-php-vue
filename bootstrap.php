<?php
declare(strict_types=1);

use App\Core\Database\Database;
use App\Core\Http\Request;

require_once "vendor/autoload.php";

Request::cors();

$db_config = [
    "HOST" => "localhost",
    "DATABASE" => "phpvue",
    "USERNAME" => "root",
    "PASSWORD" => "",
];

Database::init($db_config);

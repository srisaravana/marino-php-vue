<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\User;

require_once "../../../bootstrap.php";


try {

    Auth::authenticate();

    $id = Request::getAsInteger("id", true);

    if (!empty($id)) {
        $user = User::find($id);

        if (!empty($user)) {

            unset($user->password_hash);

            JSONResponse::validResponse(["user" => $user]);
            return;
        }
        throw new Exception("User not found");

    }

    throw new Exception("Invalid id");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}

<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\User;

require_once "../../../bootstrap.php";


try {


    Auth::authenticate();

    $fields = [
        "username" => Request::getAsString("username", true),
        "password" => Request::getAsString("password", true),
        "full_name" => Request::getAsString("full_name", true),
        "email" => Request::getAsString("email", true),
        "role" => Request::getAsString("role", true),
    ];

    if (empty($fields["username"])) throw new Exception("Username cannot be empty");
    if (empty($fields["full_name"])) throw new Exception("Full name cannot be empty");
    if (empty($fields["email"])) throw new Exception("Email cannot be empty");

    $user = User::build($fields);

    $result = $user->insert();

    if (!empty($result)) {
        $user = User::find($result);
        JSONResponse::validResponse(["user" => $user]);
        return;
    }

    throw new Exception("Failed to create user");

} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}

<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Models\User;

require_once "../../../bootstrap.php";


try {


    Auth::authenticate();

    $users = User::findAll();

    if (!empty($users)) {
        JSONResponse::validResponse(["users" => $users]);
        return;
    }
    throw new Exception("No users found!");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}

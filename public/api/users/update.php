<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\User;

require_once "../../../bootstrap.php";


try {


    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger("id", true),
        "username" => Request::getAsString("username", true),
        "full_name" => Request::getAsString("full_name", true),
        "email" => Request::getAsString("email", true),
        "role" => Request::getAsString("role", true),
    ];

    if (empty($fields["username"])) throw new Exception("Username cannot be empty");
    if (empty($fields["full_name"])) throw new Exception("Full name cannot be empty");
    if (empty($fields["email"])) throw new Exception("Email cannot be empty");

    $user = User::build($fields);

    $result = $user->update();

    if (!empty($result)) {
        JSONResponse::validResponse("User updated");
        return;
    }

    throw new Exception("Failed to create user");

} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}

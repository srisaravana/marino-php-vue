<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\User;

require_once "../../../bootstrap.php";


try {


    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger("id", true),
        "password" => Request::getAsString("password", true),
    ];

    $user = User::find($fields["id"]);

    if (!empty($user)) {

        $result = $user->updatePassword($fields["password"]);

        if ($result) {
            JSONResponse::validResponse("Password updated");
            return;
        }
        throw new Exception("Failed to update password");
    }

    throw new Exception("Invalid user");

} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
